<?php
header('Access-Control-Allow-Origin: http://localhost:3000');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Max-Age: 86400');


    //*** Data source name 
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "rudytech";


    try{
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOEXception $e) {
        echo "Connection failed: " . $e->getMessage();
        exit();
    }


?>